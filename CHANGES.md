# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleasead


## 0.2.0 - 2021-10-23

- Add class EjabberdClient, a high-level API built on top of EjabberdApi
- Use poetry build toolchain instead of setuptools
- Build Debian package
- Add CLI tool: pyjabb


## 0.1.0 - 2019-05-30

First release


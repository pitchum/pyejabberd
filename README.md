# pyejabberd

a simple python API to control a running ejabberd instance through its Rest API.

## Enabling ejabberd's Rest API

The Rest API must be enabled in *ejabberd.yml*.
Here is a posible configuration allowing full API access on port 5280 from localhost:

    listen:
      -
        port: 5280
        module: ejabberd_http
        request_handlers:
          "/api": mod_http_api
    [...]
    listen:
      -
        port: 5280
        ip: "127.0.0.1"
    [...]
    api_permissions:
      "API used from localhost allows all calls":
        - who:
          - ip: "127.0.0.1/8"
        - what:
          - "*"

More details here: https://docs.ejabberd.im/developer/ejabberd-api/simple-configuration/


## Build a Debian package

```
# Generate a valid setup.py for stdeb
poetry run dephell deps convert \
  --from-format poetry --to-format setuppy \
  --from-path pyproject.toml --to-path setup.py

# Generate .deb package
poetry run python setup.py --command-packages=stdeb.command bdist_deb

```

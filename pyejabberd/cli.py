import logging

logging.basicConfig(
    level=logging.WARN,
    format="%(levelname)-7.7s %(module)s.py:%(lineno)s \t%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
log = logging.getLogger(__name__)
import click
from pprint import pprint
from pyejabberd.api import EjabberdClient


@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Enable verbose output. Can be used twice to increase verbosity.",
)
@click.option(
    "-n",
    "--dry-run",
    is_flag=True,
)
@click.option(
    "-c",
    "--config",
    type=click.File("r"),
)
@click.option(
    "--vhost",
    help="ejabberd virtualhost to interact with."
)
@click.group()
@click.pass_context
def main(ctx, verbose=False, dry_run=False, config=None, **kwargs):
    if verbose:
        if verbose == 1:
            logging.getLogger().setLevel(logging.INFO)
            logging.getLogger().handlers[0].setFormatter(logging.Formatter('%(levelname)-8.8s %(message)s'))
            log.setLevel(logging.INFO)
        elif verbose > 1:
            logging.getLogger().setLevel(logging.DEBUG)
            logging.getLogger().handlers[0].setFormatter(logging.Formatter('%(levelname)-8.8s %(asctime)-19.19s [%(module)s.py l.%(lineno)s] %(message)s'))
            log.setLevel(logging.DEBUG)
#    if not config:
#        click.echo("Missing configuration file", err=True)
#        raise click.BadOptionUsage("config", "No configuration found")
    ctx.ensure_object(dict)
    ctx.obj["verbose"] = verbose
    ctx.obj["dry-run"] = dry_run
    ctx.obj["vhost"] = kwargs.get("vhost")
#    ctx.obj["config"] = config.name


@main.group()
@click.pass_context
def bookmark(ctx):
    """Manage user bookmarks"""
    pass


@bookmark.command(name="show")
@click.argument("username")
@click.pass_context
def bookmark_show(ctx, username):
    """Print a user's bookmarks"""
    cli = EjabberdClient(ctx.obj["vhost"])
    ret = cli.get_bookmarks(username)
    pprint(ret)

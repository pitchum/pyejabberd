import sys
import json
import requests
import xml.etree.ElementTree as ET
from collections import OrderedDict
import time

import logging
log = logging.getLogger(__name__)


class EjabberdClient(object):
    '''Toolbox for interacting with ejabberd's Rest API.
    Cf. https://docs.ejabberd.im/developer/ejabberd-api/admin-api/
    See also https://docs.ejabberd.im/developer/ejabberd-api/simple-configuration/
    '''

    def __init__(self, default_host, baseurl='http://127.0.0.1:5280/api'):
        self.baseurl = baseurl
        self.default_host = default_host
        self.api = EjabberdApi(api_vhost=default_host, baseurl=baseurl)


    def get_bookmarks(self, username):
        return [{
            "room_jid": el.attrib["jid"],
            "room_alias": el.attrib.get("name"),
            "autojoin": el.attrib.get("autojoin", "true"),
            "user_alias": el.find('{storage:bookmarks}nick').text if el.find('{storage:bookmarks}nick') else username,
            "minimize": el.find('{xmpp:gajim.org/bookmarks}minimize').text if el.find('{xmpp:gajim.org/bookmarks}minimize') else "false",
            } for el in self._iter_bookmarks(username)]


    def _iter_bookmarks(self, username):
        '''Iterate over currently registered bookmarks for a user.'''
        r = self.api.private_get(username, self.default_host, "storage", "storage:bookmarks")
        tree = ET.fromstring(r['res'])

        for el in tree.find('{storage:bookmarks}storage'):
            yield el


    def pretty_print_bookmarks(self, username):
        '''Probably useless function, except for debugging purposes'''
        r = ""
        for bm in self._iter_bookmarks(username):
            r += ("  '{}' => '{}' \n".format(bm.attrib["name"], bm.attrib["jid"]))
        return r


    def pretty_print_room_options(self, muc_name):
        '''Probably useless function, except for debugging purposes'''
        if '@' in muc_name:
            muc_name, service = muc_name.split('@')
        service = "conference." + self.default_host
        print(json.dumps(self.api.get_room_options(muc_name, service), indent=2))


    def set_bookmarks_ng(self, user, bookmarks):
        '''
        Override bookmarks registered for a user.

        Bookmarks are stored both in "private_storage" (XEP-0049) and
        via PubSub (XEP-0223).
        '''
        item_template = """
                 <conference jid="{room_jid}" autojoin="1" name="{room_alias}">
                    <nick>{user_alias}</nick>
                 </conference>"""

        existing_bookmarks = {bm.get('room_jid'): bm for bm in self.get_bookmarks(user)}

        storage_xmlfragment = '''<storage xmlns="storage:bookmarks">'''
        for bookmark in bookmarks:
            if bookmark.get('room_jid') in existing_bookmarks:
                existing_bookmarks.pop(bookmark.get('room_jid'))
            storage_xmlfragment += item_template.format(**bookmark)

        # Re-add existing extra bookmarks
        for room_jid, bookmark in existing_bookmarks.items():
            log.info("Preserving existing bookmark for '{0}': '{1}'".format(user, room_jid))
            storage_xmlfragment += item_template.format(**bookmark)
        storage_xmlfragment += '''</storage>'''

        count = len(bookmarks) + len(existing_bookmarks)

        # Store bookmarks in private storage (old way)
        ret1 = self._set_bookmarks_via_private_storage(user, storage_xmlfragment)

        # Also store bookmarks in PEP (new way)
        user_jid = user + '@' + self.default_host
        ret2 = self._set_bookmarks_via_pep(user_jid, storage_xmlfragment)

        if ret1 and ret2:
            log.info("{0} bookmarks added for '{1}'".format(count, user))
        else:
            log.warning("Something wrong happened when trying to add {0} bookmarks for '{1}'. set_bookmarks_via_private_storage_successful={2}, set_bookmarks_via_pep_successful={3}".format(count, user, ret1, ret2))


    def _set_bookmarks_via_private_storage(self, user, storage_xmlfragment):
        ret = self.api.private_set(user, self.default_host, storage_xmlfragment)
        return ret == 0


    def _set_bookmarks_via_pep(self, user_jid, storage_xmlfragment):
        stanza = '''<iq type="set" id="asdfmlkj">
     <pubsub xmlns="http://jabber.org/protocol/pubsub">
        <publish node="storage:bookmarks">
           <item>'''
        stanza += storage_xmlfragment
        stanza += '''
           </item>
        </publish>
        <publish-options>
           <x xmlns="jabber:x:data" type="submit">
              <field var="FORM_TYPE" type="hidden">
                 <value>http://jabber.org/protocol/pubsub#publish-options</value>
              </field>
              <field var="pubsub#access_model">
                 <value>whitelist</value>
              </field>
           </x>
        </publish-options>

     </pubsub>
</iq>'''
        r = self.api.send_stanza(user_jid, user_jid, stanza)
        return r == 0


    def _bookmark_xmlfragment(self, bookmark):
        """
        Generate a fragment of XML that would look like this:
        <conference autojoin="{autojoin}" jid="{room_jid}" name="{room_alias}">
            <minimize xmlns="xmpp:gajim.org/bookmarks">{minimize}</minimize>
            <nick>{user_alias}</nick>
        </conference>
        """
        conference = ET.Element('{storage:bookmarks}conference')
        conference.attrib["autojoin"] = bookmark["autojoin"]
        conference.attrib["jid"] = bookmark["room_jid"]
        conference.attrib["name"] = bookmark["room_alias"]
        ET.SubElement(conference, '{storage:bookmarks}nick').text = bookmark["user_alias"]
        ET.SubElement(conference, '{xmpp:gajim.org/bookmarks}minimize').text = bookmark["minimize"]
        return conference


    def iter_existing_room_names(self):
        '''Iterate over currently existing muc rooms.'''
        r = self.api.muc_online_rooms(self.default_host)
        for room_name in r:
            yield room_name


    def iter_existing_room_with_details(self):
        for room_name in self.iter_existing_room_names():
            room_opts = self.api.get_room_options(room_name, self.default_host)
            yield room_name, room_opts


    def ensure_private_rooms_exist(self, room_names):
        existing_rooms = {k:v for k,v in self.iter_existing_room_with_details()}
        for room_name in room_names:
            if room_name in existing_rooms:
                # TODO Not yet implemented
                log.warning("NYI Convert to private room if needed: {0}".format(room_name))
            else:
                self.create_private_muc(room_name)


    def pretty_print_existing_rooms(self):
        '''Probably useless function, except for debugging purposes'''
        print("\n".join(self.iter_existing_room_names()))


    def pretty_print_existing_rooms_with_details(self):
        '''Probably useless function, except for debugging purposes'''
        for room_name, room_opts in self.iter_existing_room_with_details():
            print("{0}\n{1}".format(room_name, json.dumps(room_opts, indent=2)))


    def create_public_channel(self, room_name):
        '''
        Create a "Channel" (i.e. a public MUC room).
        cf. https://docs.modernxmpp.org/client/groupchat/#types-of-chat
        '''
        if room_name.lower() != room_name:
            log.warning("MUC JID should be lowercase, otherwise Movim may behave weird.")
        options = {
            "title": room_name,
            "mam": "true",
            "members_by_default": "true",
            "members_only": "false",
            "anonymous": "true",
            "persistent": "true",
            "public": "true",
        }
        r = self.api.create_room_with_opts(room_name, "conference." + self.default_host, self.default_host, options)
        return r == 0


    def convert_to_private_muc(self, room_name):
        '''
        Make sure an existing room becomes a "Group Chat" (i.e. a MUC room accessible by members only).
        cf. https://docs.modernxmpp.org/client/groupchat/#types-of-chat
        '''
        if room_name.lower() != room_name:
            log.warning("MUC JID should be lowercase, otherwise Movim may behave weird.")
        options = {
            "mam": "true",
            "members_by_default": "true",
            "members_only": "true",
            "persistent": "true",
            "public": "false",
            "allow_visitor_nickchange": "false",
        }
        for option, value in options.items():
            r = self.api.change_room_option(room_name, "conference." + self.default_host, option, value)
            if r != 0:
                log.error("Failed to set option for room {}: {}={}".format(room_name, option, value))
        return True


    def set_room_members(self, room_name, members, admins):
        for uid in members:
            if uid in admins:
                log.debug("Already admin {0}".format(uid))
                continue
            self.api.set_room_affiliation(room_name, 'conference.' + self.default_host, uid + "@" + self.default_host, affil='member')
        for uid in admins:
            self.api.set_room_affiliation(room_name, 'conference.' + self.default_host, uid + "@" + self.default_host, affil='admin')


    def create_private_muc(self, room_name):
        '''
        Create a "Group Chat" (i.e. a MUC room accessible by members only).
        cf. https://docs.modernxmpp.org/client/groupchat/#types-of-chat
        '''
        if room_name.lower() != room_name:
            log.warning("MUC JID should be lowercase, otherwise Movim may behave weird.")
        options = {
            "title": room_name,
            "mam": "true",
            "members_by_default": "true",
            "members_only": "true",
            "persistent": "true",
            "public": "false",
            "allow_visitor_nickchange": "false",
        }
        r = self.api.create_room_with_opts(room_name, "conference." + self.default_host, self.default_host, options)
        return r == 0


    def create_private_muc_with_affiliations(self, room_name, members, admins=[]):
        '''
        Create a "Group Chat" (i.e. a MUC room accessible by members only).
        cf. https://docs.modernxmpp.org/client/groupchat/#types-of-chat
        '''
        log.warning("Deprecated call to create_private_muc_with_affiliations()")
        if room_name.lower() != room_name:
            log.warning("MUC JID should be lowercase, otherwise Movim may behave weird.")
        options = {
            "title": room_name,
            "mam": "true",
            "members_by_default": "true",
            "members_only": "true",
            "persistent": "true",
            "public": "false",
            "allow_visitor_nickchange": "false",
        }
        r = self.api.create_room_with_opts(room_name, "conference." + self.default_host, self.default_host, options)
        if r != 0:
            log.error("Failed to create room {}".format(room_name))
            return False

        # Create affiliations only after we are sure the room has been created
        time.sleep(1) # TODO find a better way
        for uid in members:
            if uid in admins:
                log.debug("Already admin {0}".format(uid))
                continue
            self.api.set_room_affiliation(room_name, 'conference.' + self.default_host, uid + "@" + self.default_host, affil='member')
        for uid in admins:
            self.api.set_room_affiliation(room_name, 'conference.' + self.default_host, uid + "@" + self.default_host, affil='admin')


    def pretty_print_room_members(self, room_name):
        '''Probably useless function, except for debugging purposes'''
        payload = {
          "name": room_name,
          "service": "conference." + self.default_host,
        }
        r = self._post('/get_room_affiliations', payload)
        print(json.dumps(r.json(), indent=2))


    def make_friends(self, groupnames, usera, userb, host=None):
        '''
        :param: usera is a tuple ('jid', 'cn')
        :param: userb is a tuple ('jid', 'cn')
        :param: groupnames is str of group names separated with ';'
        '''
        if not isinstance(usera, tuple):
            usera = (usera, usera)
        if not isinstance(userb, tuple):
            userb = (userb, userb)
        host = host or self.default_host
        log.debug("Make friends : groups '{0}', {1} <=> {2}".format(groupnames, usera, userb))
        if not isinstance(groupnames, str):
            groupnames = ";".join(groupnames)
        self.api.add_rosteritem(usera[0], host, userb[0], host, userb[1], groupnames, 'both')
        self.api.add_rosteritem(userb[0], host, usera[0], host, usera[1], groupnames, 'both')



    def get_friends(self, uid, host=None):
        '''
        '''
        host = host or self.default_host
        raw_roster = self.api.get_roster(uid, host)
        r = OrderedDict()

        # Build the mapping of jid:groupnames
        for rosteritem in raw_roster:
            groupname = rosteritem.get('group')
            jid = rosteritem.get('jid')
            if jid not in r:
                r[jid] = rosteritem
                r[jid]['groupnames'] = []
                del r[jid]['group']
            if groupname not in r[jid]['groupnames']:
                r[jid]['groupnames'].append(groupname)

        return r


    def get_roster_indexed(self, uid, host=None):
        '''
        {
              "roster_group_A": {
                "alice@example.org": {
                  "group": "roster_group_A",
                  "subscription": "both",
                  "jid": "alice@example.org",
                  "nick": "alice",
                  "ask": "none"
                },
              "roster_group_B": {
                "user_b@example.com": { ... },
              ...
        }
        '''
        host = host or self.default_host
        raw_roster = self.api.get_roster(uid, host)
        r = OrderedDict()
        # First, get group definitions
        roster_groups = set()
        for rosteritem in raw_roster:
            roster_groups.add(rosteritem.get('group'))
        for groupname in sorted(roster_groups):
            r[groupname] = OrderedDict()
        # Second, populate groups
        for rosteritem in raw_roster:
            groupname = rosteritem.get('group')
            jid = rosteritem.get('jid')
            r.get(groupname)[jid] = rosteritem
        return r


    def pretty_print_roster(self, uid):
        '''Probably useless function, except for debugging purposes'''
        print("Roster of {}".format(uid))
        for groupname, contacts in self.get_roster_indexed(uid).items():
            print("[{}]".format(groupname))
            for jid, rosteritem in contacts.items():
                print("  {}".format(jid))



    # XXX shared roster management. EXPERIMENTAL
    def iter_existing_shared_rosters(self):
        for group in self.api.srg_list(self.default_host):
            print("Fetching members of shared group {0}".format(group))
            members = self.api.srg_get_members(group, self.default_host)
            yield group, members


    def create_shared_roster_group(self, groupname, members, host=None):
        host = host or self.default_host
        self.api.srg_create(groupname, host, groupname, "desc", groupname)
        for uid in members:
            self.api.srg_user_add(uid, host, groupname, host)


    def update_shared_roster_group(self, groupname, members, host=None):
        '''Add and remove existing members as needed.'''
        log.warning("Unfinished function!")
        host = host or self.default_host
        log.info("updating shared roster group {0}".format(groupname))
        existing_members = self.api.srg_get_members(groupname, host)
#        print("{0} members existing in group {1}".format(len(existing_members), groupname))
        for m in members:
            jid = m + "@" + host
            if jid not in existing_members:
#                print("Adding user {0} to shared group {1}".format(m, groupname))
                r = self.api.srg_user_add(m, host, groupname, host)
                if r:
                    log.info("Added user {0} into shared group {1}".format(m, groupname))
                else:
                    log.error("Failed to add user {0} into shared group {1}".format(m, groupname))
            else:
                existing_members.remove(jid)
        for jid in existing_members:
#            print("Removing member {}".format(jid))
            m, host = jid.split('@')
            self.api.srg_user_del(m, host, groupname, host)
            log.info("Removed user {0} from shared group {1}".format(m, groupname))


class EjabberdApi(object):
    '''Basic API for interacting with ejabberd's Rest API.
    Function names are exactly the same.
    Cf. https://docs.ejabberd.im/developer/ejabberd-api/admin-api/
    '''

    def __init__(self, api_vhost=None, baseurl='http://127.0.0.1:5280/api'):
        self.api_vhost = api_vhost # Don't remember why this is needed
        self.baseurl = baseurl

    # Internal private functions
    def _post(self, path, payload, vhost=None):
        vhost = vhost or self.api_vhost
        log.debug("POST {0} {1}".format(path, json.dumps(payload, indent=2)))
        resp = requests.post(self.baseurl + path, data=json.dumps(payload), headers= {"Host": vhost}, verify=False)
        if resp.status_code != 200:
            log.warning("HTTP STATUS {0} when calling '{1}'. Reason: {2}".format(resp.status_code, resp.request.url, resp.reason))
        return resp


    def add_rosteritem(self, localuser, localserver, user, server, nick, group, subs):
        '''Add an item to a user's roster (supports ODBC)
        Group can be several groups separated by ; for example: "g1;g2;g3"
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#add-rosteritem

        localuser :: string : User name
        localserver :: string : Server name
        user :: string : Contact user name
        server :: string : Contact server name
        nick :: string : Nickname
        group :: string : Group
        subs :: string : Subscription

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            'localuser': localuser,
            'localserver': localserver,
            'user': user,
            'server': server,
            'nick': nick,
            'group': group,
            'subs': subs,
        }
        r = self._post('/add_rosteritem', payload)
        return r.json()


    def delete_rosteritem(self, localuser, localserver, user, server):
        '''Delete an item from a user's roster (supports ODBC)
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#delete-rosteritem

        localuser :: string : User name
        localserver :: string : Server name
        user :: string : Contact user name
        server :: string : Contact server name

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            'localuser': localuser,
            'localserver': localserver,
            'user': user,
            'server': server,
        }
        r = self._post('/delete_rosteritem', payload)
        return r.json()


    def get_roster(self, user, server):
        '''Get roster of a local user
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#get-roster

        user :: string : Contact user name
        server :: string : Contact server name

        :return: contacts :: [{jid::string, nick::string, subscription::string, ask::string, group::string}]
        '''
        payload = {
            'user': user,
            'server': server,
        }
        r = self._post('/get_roster', payload)
        return r.json()


    # Private storage
    def private_get(self, user, host, element, ns):
        '''Get some information from a user private storage
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#private-get

        :return: res :: string
        '''
        payload = {
            'user': user,
            'host': host,
            'element': element,
            'ns': ns,
        }
        r = self._post('/private_get', payload)
        return r.json()


    def private_set(self, user, host, element):
        '''Set to the user private storage
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#private-get

        :return: res :: string
        '''
        payload = {
            'user': user,
            'host': host,
            'element': element,
        }
        r = self._post('/private_set', payload)
        return r.json()


    # Stanza (generic)
    def send_stanza(self, from_, to, stanza):
        '''Send a stanza; provide From JID and valid To JID
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#send-stanza

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            'from': from_,
            'to': to,
            'stanza': stanza,
        }
        r = self._post('/send_stanza', payload)
        return r.json()


    def send_stanza_c2s(self, from_, to, stanza):
        '''Send a stanza as if sent from a c2s session
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#send-stanza-c2s

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            'user': user,
            'host': host,
            'resource': resource,
            'stanza': stanza,
        }
        r = self._post('/send_stanza_c2s', payload)
        return r.json()


    # Shared roster
    def srg_create(self, group, host, name, description, display):
        '''Create a Shared Roster Group
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-create

        If you want to specify several group identifiers in the Display
        argument, put \" around the argument and separate the
        identifiers with \n. For example:
        ejabberdctl srg_create group3 myserver.com name desc \"group1\ngroup2\"
        '''
        payload = {
            'group': group,
            'host': host,
            'name': name,
            'description': description,
            'display': display,
        }
        r = self._post('/srg_create', payload)
        return r.json()


    def srg_delete(self, group, host):
        '''Delete a Shared Roster Group
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-delete
        '''
        payload = {
            'group': group,
            'host': host,
        }
        r = self._post('/srg_delete', payload)
        return r.json()


    def srg_get_info(self, group, host):
        '''Get info of a Shared Roster Group
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-get-info

        :return: informations :: [{key::string, value::string}] : List of group informations, as key and value
        '''
        payload = {
            'group': group,
            'host': host,
        }
        r = self._post('/srg_get_info', payload)
        return r.json()


    def srg_get_members(self, group, host):
        '''Get members of a Shared Roster Group
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-get-members

        :return: members :: [member::string] : List of group identifiers
        '''
        payload = {
            'group': group,
            'host': host,
        }
        r = self._post('/srg_get_members', payload)
        return r.json()


    def srg_list(self, host):
        '''List the Shared Roster Groups in Host
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-list

        :return: groups :: [id::string] : List of group identifiers
        '''
        payload = { 'host': host }
        r = self._post('/srg_list', payload)
        return r.json()


    def srg_user_add(self, user, host, group, grouphost):
        '''Add the JID user@host to the Shared Roster Group
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-user-add

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            'user': user,
            'host': host,
            'group': group,
            'grouphost': grouphost,
        }
        r = self._post('/srg_user_add', payload)
        return r.json()


    def srg_user_del(self, user, host, group, grouphost):
        '''Delete this JID user@host from the Shared Roster Group
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#srg-user-del

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            'user': user,
            'host': host,
            'group': group,
            'grouphost': grouphost,
        }
        r = self._post('/srg_user_del', payload)
        return r.json()


    # MUC
    def create_room_with_opts(self, name, service, host, options):
        '''Create a MUC room name@service in host with given options
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#create-room-with-opts

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            "name": name,
            "service": service,
            "host": host,
            "options": options,
        }
        r = self._post('/create_room_with_opts', payload)
        return r.json()


    def change_room_option(self, name, service, option, value):
        '''Change an option in a MUC room
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#change-room-option

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
            "name": name,
            "service": service,
            "option": option,
            "value": value,
        }
        r = self._post('/change_room_option', payload)
        return r.json()


    def muc_online_rooms(self, host):
        '''List existing rooms ('global' to get all vhosts)
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#muc-online-rooms

        :return: rooms :: [room::string] : List of rooms
        '''
        payload = {
          "host": host,
        }
        r = self._post('/muc_online_rooms', payload)
        return r.json()


    def destroy_room(self, room_name):
        '''Destroy a MUC room
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#destroy-room

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
          "name": room_name,
          "service": "conference." + self.default_host,
        }
        r = self._post('/destroy_room', payload)


    def set_room_affiliation(self, name, service, jid, affil):
        '''Change an affiliation in a MUC room
        https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#set-room-affiliation

        :return: res :: integer : Status code (0 on success, 1 otherwise)
        '''
        payload = {
          "name": name,
          "service": service,
          "jid": jid,
          "affiliation": affil,
        }
        r = self._post('/set_room_affiliation', payload)


    def get_room_options(self, name, service):
        payload = {
            "name": name,
            "service": service,
        }
        r = self._post('/get_room_options', payload)
        return r.json()
